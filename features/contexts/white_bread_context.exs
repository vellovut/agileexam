defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Auction.{Repo, AuctionItem, User}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Auction.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Auction.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Auction.Repo)
    Hound.end_session
  end

  given_ ~r/^the following auctions are open$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn auction_item -> AuctionItem.changeset(%AuctionItem{}, auction_item) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    [%{username: "Fred"},
      %{username: "Bilbo"},
      %{username: "Gandalf"},
      %{username: "Frodo"} ]
    |>  Enum.map(fn item -> User.changeset(%User{}, item) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    {:ok, state}
  end

  and_ ~r/^I select "(?<argument_one>[^"]+)"$/, fn state, %{argument_one: argument_one} ->
    navigate_to "/#/"
    click({:id, "select-" <> argument_one})
    {:ok, state}
  end

  and_ ~r/^I enter the bid "(?<argument_one>[^"]+)"$/, fn state, %{argument_one: argument_one} ->
    fill_field({:id, "bid-input"}, argument_one)
    {:ok, state}
  end

  when_ ~r/^I submit the bid$/, fn state ->
    click({:id, "submit"})
    {:ok, state}
  end

  and_ ~r/^The item price is updated to "(?<argument_one>[^"]+)"$/, fn state, %{argument_one: argument_one} ->
    :timer.sleep(2000)
    price_label= find_element(:id, "price-label")
    assert inner_text(price_label) == argument_one
    {:ok, state}
  end


  then_ ~r/^I should be notified "(?<argument_one>[^"]+)"$/, fn state, %{argument_one: argument_one} ->
    assert visible_in_page? Regex.compile!(argument_one)
    {:ok, state}
  end

end
