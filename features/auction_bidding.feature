Feature: Auction bidding
  As an user
  I want to select the preferred auction
            and make a bid on it.
  Scenario Outline: Bidding via web app
    Given the following auctions are open
          | description                             | price   | closing_date  |
          | Chicago Bullet Speed Skate (Size 7)     | 59.00   | 2018-10-24    |
          | Riedell Dart Derby Skates (Size 8)      | 106.00  | 2018-01-19    |
          | Roller Derby Brand Blade Skate (Size 7) | 112.00  | 2018-01-12    |
    And I select "<item>"
    And I enter the bid "<bid>"
    When I submit the bid
    And The item price is updated to "<price_after_bid>"
    Then I should be notified "<notification>"

    Examples:
      | item                                | bid     | price_after_bid |  notification                    |
      | Chicago Bullet Speed Skate (Size 7) | 62.00   | 62.00           | Your bid has been accepted!      |
      | Riedell Dart Derby Skates (Size 8)  | 100.00  | 106.00          |Your bid has been rejected!      |
