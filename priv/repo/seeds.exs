# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Auction.Repo.insert!(%Auction.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will halt execution if something goes wrong.
alias Auction.{Repo, AuctionItem, User}

[%{description: "xxx", price: 21.00, closing_date: "2018-10-24"},
  %{description: "Item 2", price: 51.00, closing_date: "2018-01-19"},
  %{description: "Item 3", price: 91.00, closing_date: "2018-01-10"},
  %{description: "Item 3", price: 91.00, closing_date: "2018-01-09"}]
|> Enum.map(fn item -> AuctionItem.changeset(%AuctionItem{}, item) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%{username: "Fred"},
  %{username: "Bilbo"},
  %{username: "Gandalf"},
  %{username: "Frodo"} ]
|>  Enum.map(fn item -> User.changeset(%User{}, item) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
