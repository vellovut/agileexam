defmodule Auction.Repo.Migrations.AddAuctionItem do
  use Ecto.Migration

  def change do
    create table(:auction_items) do
      add :description, :string
      add :price, :float
      add :closing_date, :date

      timestamps()
    end
  end
end
