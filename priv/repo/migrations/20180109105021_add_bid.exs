defmodule Auction.Repo.Migrations.AddBid do
  use Ecto.Migration

  def change do
    create table(:bids) do
      add :amount, :float
      add :user_id, references(:users)
      add :auction_item_id, references(:auction_items)

      timestamps()
    end

    create index(:bids, [:user_id])
    create index(:bids, [:auction_item_id])
  end
end
