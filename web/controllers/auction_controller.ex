defmodule Auction.AuctionController do
  use Auction.Web, :controller
  alias Auction.{Repo, AuctionItem, User, Bid}
  alias Ecto.{Changeset}

  def index(conn, _params) do
    items = Repo.all(AuctionItem)

    put_status(conn, 200)
    |> json items
  end

  def show(conn, %{"id" => auction_item_id} = params) do
    item = Repo.get(AuctionItem, auction_item_id)
    if (item == nil) do
      put_status(conn, 400)
      |> json %{message: "Auction item not found!"}
    else
      put_status(conn, 200)
      |> json item
    end
  end

  def bid(conn,  %{"id" => auction_item_id, "bid" => bid, "user_id" => user_id} = params) do
    item = Repo.get(AuctionItem, auction_item_id)
    user = Repo.get(User, user_id)

    if (item.price < bid) do
      item = Ecto.Changeset.change item, price: bid / 1
      case Repo.update item do
        {:ok, item}       ->

          bid_cs =
            Bid.changeset(%Bid{}, %{amount: bid})
            |> Changeset.put_change(:user_id, user.id)
            |> Changeset.put_change(:auction_item_id, item.id)
          Repo.insert(bid_cs)

          query = from b in Bid, where: b.auction_item_id == ^auction_item_id, preload: [:user, :auction_item], order_by: [desc: b.inserted_at]
          history = Repo.all(query)

          put_status(conn, 200)
          |> json %{message: "Your bid has been accepted!", item: item, history: history}
        {:error, changeset} -> put_status(conn, 400) |> json %{message: "Your bid has been rejected!"}
      end
    else
      put_status(conn, 400) |> json %{message: "Your bid has been rejected!"}
    end
  end

  def bid_history(conn,%{"id" => auction_item_id } = params ) do
    query = from b in Bid, where: b.auction_item_id == ^auction_item_id, preload: [:user, :auction_item], order_by: [desc: b.inserted_at]
    history = Repo.all(query)

    put_status(conn, 200)
    |> json history
  end

end
