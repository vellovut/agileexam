defmodule Auction.UserController do
  use Auction.Web, :controller
  alias Auction.{Repo, User}
  
  def index(conn, _params) do
    users = Repo.all(User)

    put_status(conn, 200)
    |> json users
  end

end
