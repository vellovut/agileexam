import Vue from "vue";
import VueRouter from "vue-router";

import Main from "./main";
import AuctionItem from "./auction_item";

import "phoenix";
import "axios";

Vue.use(VueRouter);

var router = new VueRouter({
    routes: [
        { path: '/', component: Main},
        { path: '/auction-item/:id', component: AuctionItem},
        { path: '*', redirect: '/' }
    ]
});

new Vue({
    router
}).$mount("#auction-app");