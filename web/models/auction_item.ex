defmodule Auction.AuctionItem do
  use Auction.Web, :model

  @derive {Poison.Encoder, only: [:id, :description, :price, :closing_date]}
  schema "auction_items" do
    field :description, :string
    field :price, :float
    field :closing_date, :date
    has_many :bids, Auction.Bid

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:description, :price, :closing_date])
    |> validate_required([:description, :price, :closing_date])
  end
end
