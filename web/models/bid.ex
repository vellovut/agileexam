defmodule Auction.Bid do
    use Auction.Web, :model

    @derive {Poison.Encoder, only: [:id, :user, :auction_item, :amount, :inserted_at]}
    schema "bids" do
      field :amount, :float
      belongs_to :user, Auction.User, foreign_key: :user_id
      belongs_to :auction_item, Auction.AuctionItem, foreign_key: :auction_item_id

      timestamps()
    end

    def changeset(struct, params \\ %{}) do
      struct
      |> cast(params, [:amount])
      |> validate_required([:amount])
    end
end
