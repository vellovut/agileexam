defmodule Auction.User do
  use Auction.Web, :model

  @derive {Poison.Encoder, only: [:id, :username]}
  schema "users" do
    field :username, :string
    has_many :bids, Auction.Bid
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:username])
    |> validate_required([:username])
  end
end
