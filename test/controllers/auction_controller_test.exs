defmodule Auction.AuctionControllerTest do
  use Auction.ConnCase
  alias Auction.{Repo, AuctionItem, User}



  test "POST /auctions (successful bid)", %{conn: conn} do
    usercs = User.changeset(%User{}, %{username: "fred"})
    user = Repo.insert!(usercs)

    changeset = AuctionItem.changeset(%AuctionItem{}, %{description: "Riedell Dart Derby Skates (Size 8)", price: 21.00, closing_date: "2018-10-10"})
    item = Repo.insert!(changeset)

    conn = post conn, "/api/auctions", %{id: item.id, bid: 22, user_id: user.id}
    assert json_response(conn, 200)
  end

  test "POST /auctions (rejected bid)", %{conn: conn} do
    usercs = User.changeset(%User{}, %{username: "fred"})
    user = Repo.insert!(usercs)
    changeset = AuctionItem.changeset(%AuctionItem{}, %{description: "Riedell Dart Derby Skates (Size 8)", price: 21.00, closing_date: "2018-10-10"})
    item = Repo.insert!(changeset)

    conn = post conn, "/api/auctions", %{id: item.id, bid: 20,user_id: user.id}
    assert json_response(conn, 400)
  end

end
